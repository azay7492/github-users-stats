const initialState = {
  usersList: [],
  repos:[],
};

function rootReducer(state = initialState, action) {
  if (action.type == "SET_USERS") {
    return Object.assign({}, state, {
      usersList: action.payload,
    });
  } else if (action.type == "SET_REPOS") {
    return Object.assign({}, state, {
      repos: action.payload,
    });
  }
  return state;
}

export default rootReducer;
