import React from "react";
import { TouchableOpacity, Image } from "react-native";
var commonStyle = require("../common/styles");

const HeaderLeft = (props) => {
  return (
    <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
      <Image
        style={[commonStyle.drawerTogglerImage]}
        source={require("../assests/icons/menu.png")}
      ></Image>
    </TouchableOpacity>
  );
};

export default HeaderLeft;
