import React from "react";
import { SafeAreaView, StatusBar, ScrollView, View } from "react-native";
import commonStyle from "../../utils/commonStyle";
import Spinner from "../Spinner";
import Footer from "../Footer";

const Layout = (props) => {
  return (
    <SafeAreaView style={[commonStyle.container]}>
      <Spinner spinner={props.spinner ? props.spinner : false} />
      <StatusBar
        backgroundColor={"transparent"}
        translucent={true}
        barStyle="dark-content"
      />

      {props.scroll ? (
        <ScrollView
          style={[commonStyle.body]}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        >
          {props.children}
        </ScrollView>
      ) : (
        <View style={[commonStyle.body]}>{props.children}</View>
      )}
     
    </SafeAreaView>
  );
};

export default Layout;
