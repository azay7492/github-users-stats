import React from "react";
import { ActivityIndicator, StyleSheet, View, Dimensions } from "react-native";

const Spinner = (props) => {
  return (
    <React.Fragment>
      {props.spinner ? (
        <View style={[styles.container]}>
          <ActivityIndicator
            animating={props.spinner}
            size="large"
            color="#e6131a"
          />
        </View>
      ) : (
        <View />
      )}
    </React.Fragment>
  );
};

export default Spinner;

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    backgroundColor: "#000",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 9,
  },
});
