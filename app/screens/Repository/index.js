import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image, FlatList,TextInput } from "react-native";
import { useDispatch, useSelector } from "react-redux";

import { Layout } from "../../common";
import { API } from "../../utils/Api";

var commonStyle = require("../../utils/commonStyle");
var s = require("./style");

function Repository({route}) {
  const [spinner, setSpinner] = useState(true);
  const state = useSelector((state) => state.repos);
  const dispatch = useDispatch();

  useEffect(() => {
    const userRepoUrl =`/users/${route.params.userID}/repos`
    API(userRepoUrl, async (response)=> {
      dispatch({ type: "SET_REPOS", payload: response });
      setSpinner(false);
    });
  }, [route.params.userID]);

  const renderRepos = ({ item }) => (
    <TouchableOpacity
    style={{paddingVertical:5}}>
      <View style={s.repoContainer}>
       
        <Text style={s.textColor}>
          {item.name}
        </Text>
        <View style={s.textContainer}>
          <Text style={s.textColor}>commits : {item.forks_count}</Text>
          <Text style={s.textColor}>Forks : {item.forks_count}</Text>
          <Text style={s.textColor}>Issues : {item.open_issues_count}</Text>
        </View>
      
      </View>
    </TouchableOpacity>
  );

  return (
    <Layout spinner={spinner}>
        <View style={[commonStyle.row,{marginTop:15}]}>
          <FlatList
            data={state}
            renderItem={renderRepos}
            keyExtractor={(item) => '"' + item.id + '"'}
          />
        </View>
    </Layout>
  );
}
export default Repository;
