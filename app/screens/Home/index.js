import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image, FlatList,TextInput } from "react-native";
import { useDispatch, useSelector } from "react-redux";

import { Layout } from "../../common"
import { API } from "../../utils/Api";
import {DEVICE_WIDTH} from '../../utils/Constants'

var commonStyle = require("../../utils/commonStyle");
var s = require("./style");

function Home(props) {
  const [spinner, setSpinner] = useState(true);
  const [query, setQuery] = useState(null);
  const state = useSelector((state) => state.usersList);
  const dispatch = useDispatch();

  useEffect(() => {
    const userListUrl = query ? `/search/users?q=${query}` : '/users'
    API(userListUrl, async (response)=> {
      dispatch({ type: "SET_USERS", payload: query ? response.items: response });
      setSpinner(false);
    });
  }, [query]);

  const renderUser = ({ item }) => (
    <TouchableOpacity
      style={[s.imageContainer,{width:(DEVICE_WIDTH/3) - 10}]}
      onPress={() =>
        props.navigation.navigate("Repository", { userID: item.login })
      }
    >
      <Image
        style={[s.userImage]}
        source={{ uri:item.avatar_url }}
      />
      <View style={s.textContainer}>
        <Text style={{ width: "100%", textAlign: "center" }}>
          {item.login}
        </Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <Layout spinner={spinner}>
      <TextInput
        style={s.textInput} 
        onChangeText={(val)=>setQuery(val)}
        placeholder="Search users..."
        placeholderTextColor="red"
      />
        <View style={[commonStyle.row,{marginTop:15}]}>
          <FlatList
            data={state}
            renderItem={renderUser}
            keyExtractor={(item) => '"' + item.id + '"'}
            numColumns={3} 
          />
        </View>
    </Layout>
  );
}
export default Home;
