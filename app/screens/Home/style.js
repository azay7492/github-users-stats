"use strict";
var React = require("react-native");
var { StyleSheet } = React;

module.exports = StyleSheet.create({
  textInput:{borderColor:"red",borderWidth:1,borderRadius:5, color:"red",paddingHorizontal:20},
  imageContainer: {
    height: 120,
    width: 150,
    marginRight: 5,
    borderRadius: 10,
    overflow: "hidden",
    position: "relative",
    marginBottom: 15,
  },
  userImage: {
    height: "100%",
    width: "100%",
  },
  textContainer:{
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%",
    backgroundColor: "rgba(255,255,255,0.5)",
    paddingVertical: 10,
  }
});
