import { Dimensions } from "react-native";
export const API_BASE_URL = "https://api.github.com";

export const COLORS = {
  APP_COLOR: "#e2322e",
};

export const DEVICE_WIDTH = Dimensions.get("window").width;
export const DEVICE_HEIGHT = Dimensions.get("window").height;
