"use strict";
import { StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  container: { flex: 1, position: "relative", backgroundColor: "#000" },
  body: { flex: 1, padding: 10, backgroundColor: "#000" },
  row: { flexDirection: "row" },
  textUpper: { textTransform: "uppercase", color: "red" },
  mt5: { marginTop: 5 },
  mt10: { marginTop: 10 },
  mt15: { marginTop: 15 },
  mt20: { marginTop: 20 },
  mt25: { marginTop: 25 },
});
