import * as React from "react";
import { Alert} from "react-native";
export const navigationRef = React.createRef();

export function navigate(name, params) {
  if (name) {
    navigationRef.current?.navigate(name, params);
  } else {
    Alert.alert("Notice", "Currently Not Available");
  }
}

export function goBack() {
  navigationRef.current?.goBack();
}